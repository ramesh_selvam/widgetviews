// Created by bryankeller on 11/30/18.
// Copyright © 2018 Airbnb, Inc.

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import MagazineLayout
import UIKit

protocol MagazineLayoutCollectionViewCellDelegate{
    
    func didTapAddButton(indexPath:IndexPath)
    func didTapDeleteButton(indexPath:IndexPath)
}

final class Cell: MagazineLayoutCollectionViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    var indexPath: IndexPath?
    var delegate: MagazineLayoutCollectionViewCellDelegate?
    // MARK: Lifecycle

//  override init(frame: CGRect) {
//    label = UILabel(frame: .zero)
//    imageView = UIImageView(frame: .zero)
//    super.init(frame: frame)
//
//    if !UIAccessibility.isReduceTransparencyEnabled {
//        self.backgroundColor = .clear
//
//        let blurEffect = UIBlurEffect(style: .extraLight)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        //always fill the view
//        blurEffectView.layer.cornerRadius = 20
//        blurEffectView.layer.borderWidth = 2
////        blurEffectView.layer.borderColor = UIColor.red.
//
//        blurEffectView.frame = self.bounds
//        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//
//        self.addSubview(blurEffectView)
//        //if you have more UIViews, use an insertSubview API to place it where needed
//    } else {
//        self.backgroundColor = .white
//
//    }
//
//    contentView.addSubview(imageView)
//
//    label.font = UIFont.systemFont(ofSize: 45)
//    label.textColor = .white
//    label.numberOfLines = 0
//    label.textAlignment = .center
//    contentView.addSubview(label)
//
//    label.translatesAutoresizingMaskIntoConstraints = false
//    NSLayoutConstraint.activate([
//      label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
//      label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),
//      label.topAnchor.constraint(equalTo: contentView.topAnchor),
//      label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
//    ])
//
//    imageView.translatesAutoresizingMaskIntoConstraints = false
//    NSLayoutConstraint.activate([
//        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
//        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
//        imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
//        imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
//    ])
//
//
//
//  }
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        self.delegate?.didTapDeleteButton(indexPath: indexPath!)
    }
    @IBAction func didTapAddButton(_ sender: Any) {
        self.delegate?.didTapAddButton(indexPath: indexPath!)
    }
    // MARK: Internal

  override func prepareForReuse() {
    super.prepareForReuse()

//    label.text = nil
    contentView.backgroundColor = nil
  }

  func set(_ itemInfo: ItemInfo) {
    label.text = itemInfo.text
    contentView.backgroundColor = itemInfo.color
  }

  // MARK: Private

//    private let label: UILabel
//    private let imageView: UIImageView

}
